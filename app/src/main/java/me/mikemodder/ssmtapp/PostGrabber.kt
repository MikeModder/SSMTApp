package me.mikemodder.ssmtapp

import android.os.AsyncTask
import android.util.Log
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.Request

/**
 * Created by mike on 5/19/18.
 */

class PostGrabber : AsyncTask<String, Void, ArrayList<String>>()
{
    val TAG = "PostGrabber"
    override fun doInBackground(vararg apiBase: String?): ArrayList<String>? {
        val client = OkHttpClient()
        val gsonBuilder = GsonBuilder()
        val gson = gsonBuilder.create()
        val apiUrl = apiBase.first() + "posts"
        Log.d(TAG, "Preparing to fetch data with API URL: %s".format(apiUrl))
        try {
            val req = Request.Builder()
                    .url(apiUrl)
                    .build()
            val res = client.newCall(req).execute()
            val resString = res.body()!!.string()
            val resJson = gson.fromJson(resString, ArrayList<String>().javaClass)
            Log.d(TAG, "Got the data okay")
            Log.d(TAG, "Result body: %s".format(resString))
            return@doInBackground resJson
        } catch (e: Throwable){
            Log.d(TAG, "Something broke ;-;")
            e.printStackTrace()
            return null
        }

    }

    override fun onPostExecute(result: ArrayList<String>?) {
        super.onPostExecute(result)
        Log.d(TAG, "Finished grabbing posts. Data: %s".format(result.toString()))
    }
}