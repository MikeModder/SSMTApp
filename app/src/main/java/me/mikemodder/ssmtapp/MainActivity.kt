package me.mikemodder.ssmtapp

import android.app.Dialog
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.auth0.android.Auth0
import com.auth0.android.authentication.AuthenticationException
import com.auth0.android.provider.AuthCallback
import com.auth0.android.provider.WebAuthProvider
import com.auth0.android.result.Credentials
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    val TAG = "MainActivity"
    val EXTRAS_ACCESS_TOKEN = "me.mikemodder.ssmtapp.ACCESS_TOKEN"
    val EXTRAS_EXPRES_AT = "me.mikemodder.ssmtapp.EXPIRES_AT"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        loginBtn.setOnClickListener {
            login()
        }

    }

    private fun login(){
        val auth0 = Auth0(super.getApplicationContext())
        WebAuthProvider.init(auth0)
                .withScheme("ssmt")
                .withAudience("https://%s/userinfo".format(getString(R.string.com_auth0_domain)))
                .start(this,  object: AuthCallback{
                    override fun onFailure(dialog: Dialog) {
                        // I guess we're supposed to show a dialog
                        runOnUiThread { dialog.show() }
                    }

                    override fun onFailure(exception: AuthenticationException?) {
                        // I don't know what to do here
                        runOnUiThread { Toast.makeText(this@MainActivity, "There was an error while authenticating!\n%s".format(exception.toString()), Toast.LENGTH_LONG).show() }
                    }

                    override fun onSuccess(credentials: Credentials) {
                        // wtaf it worked???
                        val intent = Intent(this@MainActivity, PostLoginActivity::class.java).apply { putExtra(EXTRAS_ACCESS_TOKEN, credentials.accessToken); putExtra(EXTRAS_EXPRES_AT, credentials.expiresAt.toString())}
                        runOnUiThread { Toast.makeText(this@MainActivity, "Login successful!", Toast.LENGTH_SHORT).show(); startActivity(intent) }
                    }
                })
    }
}
