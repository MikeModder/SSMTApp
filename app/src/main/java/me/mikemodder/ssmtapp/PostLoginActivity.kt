package me.mikemodder.ssmtapp

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.postlogin.*

class PostLoginActivity : AppCompatActivity() {
    val TAG = "PostLoginAct"
    val EXTRAS_ACCESS_TOKEN = "me.mikemodder.ssmtapp.ACCESS_TOKEN"
    val EXTRAS_EXPRES_AT = "me.mikemodder.ssmtapp.EXPIRES_AT"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.postlogin)
        val accToken = intent.getStringExtra(EXTRAS_ACCESS_TOKEN)
        val expiresAt = intent.getStringExtra(EXTRAS_EXPRES_AT)
        plTxt.text = "Token: %s\nExoires: %s".format(accToken, expiresAt)
    }
}
